package com.codeikram.userservice.service;

import com.codeikram.userservice.VO.Department;
import com.codeikram.userservice.VO.ResponseTemplateVO;
import com.codeikram.userservice.entity.User;
import com.codeikram.userservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RestTemplate restTemplate;

    public User saveUser(User user) {
        log.info("Inside saveUser methode of userService");
        return userRepository.save(user);
    }

    public ResponseTemplateVO getUserWithDepartment(Long userId) {
        log.info("Inside ResponseTemplateVO methode of userService");
        ResponseTemplateVO vo = new ResponseTemplateVO();
        User user = userRepository.findById(userId).get();
        Department department = restTemplate.getForObject("http://DEPARTEMENT-SERVICE/department/"+user.getDepartmentId(), Department.class);
        vo.setUser(user);
        vo.setDepartment(department);
        return vo;
    }

//    public User findUserById(Long userId) {
//        log.info("Inside saveUser methode of userService");
//        return userRepository.findById(userId).get();
//    }



}
