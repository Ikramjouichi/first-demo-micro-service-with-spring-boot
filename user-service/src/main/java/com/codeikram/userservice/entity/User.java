package com.codeikram.userservice.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "app_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;
    private String firstName;
    private String lastName;
    private  String email;
    private Long departmentId;
}
/*
{
    "user": {
        "userId": 1,
        "firstName": "ikram",
        "lastName": "jouichi",
        "email": "ikram@gmail.com",
        "departmentId": 1
    },
    "department": {
        "departmentId": 1,
        "departmentName": "IT",
        "departmentAddress": "Guiliz ,Marrakech",
        "departmentCode": "IT-006"
    }
}
 */