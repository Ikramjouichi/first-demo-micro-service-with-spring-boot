package com.codeikram.userservice.controller;

import com.codeikram.userservice.VO.ResponseTemplateVO;
import com.codeikram.userservice.entity.User;
import com.codeikram.userservice.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;
    @PostMapping("/")
    public User saveUser(@RequestBody User user){
        log.info("Inside saveUser methode of userController");
        return userService.saveUser(user);
    }

    @GetMapping("/{userId}")
    public ResponseTemplateVO getUserWithDepartment(@PathVariable  Long userId){
        return userService.getUserWithDepartment(userId);

    }



//    @GetMapping("/{userId}")
//    public  User findUserById(@PathVariable Long userId){
//        log.info("Inside findUserById  methode of userController");
//        return userService.findUserById(userId);
//    }
}
