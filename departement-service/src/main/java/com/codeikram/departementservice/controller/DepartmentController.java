package com.codeikram.departementservice.controller;

import com.codeikram.departementservice.entity.Department;
import com.codeikram.departementservice.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/department")
@Slf4j
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/")
    public Department saveDepartment(@RequestBody Department department){
        log.info("Inside saveDepartment methode of departmentController");
        return departmentService.saveDepartment(department);
    }

    @GetMapping("/{departmentId}")
    public  Department findDepartmentById(@PathVariable Long departmentId){
        log.info("Inside saveDepartment methode of departmentController");
        return departmentService.findDepartmentById(departmentId);
    }

}
