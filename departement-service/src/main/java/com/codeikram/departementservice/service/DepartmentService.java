package com.codeikram.departementservice.service;

import com.codeikram.departementservice.entity.Department;
import com.codeikram.departementservice.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;

    public Department saveDepartment(Department department) {
        log.info("Inside saveDepartment methode of departmentService");
        return departmentRepository.save(department);
    }
    public Department findDepartmentById(Long departmentId){
        log.info("Inside findDepartmentById methode of departmentService");
        return departmentRepository.findById(departmentId).get() ;
    }
}
